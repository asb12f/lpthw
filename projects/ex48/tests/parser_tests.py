from nose.tools import *
from ex48 import parser

def test_peek():
	assert_equal(parser.peek([('noun', 'princess')]), 'noun')
	assert_equal(parser.peek([('noun', 'princess'),('verb', 'run')]), 'noun')
	assert_equal(parser.peek([]), None)
	
def test_match():
	assert_equal(parser.match([('noun', 'princess')], 'noun'), ('noun', 'princess'))
	assert_equal(parser.match([('noun', 'princess'), ('verb', 'run')], 'noun'), ('noun', 'princess'))
	assert_equal(parser.match([('noun', 'princess')], 'verb'), None)
	assert_equal(parser.match([], 'verb'), None)
	
def test_parse_verb():
	assert_equal(parser.parse_verb([('stop', 'princess'), ('verb', 'run'), ('direction', 'north')]), ('verb', 'run'))
	assert_raises(parser.ParserError, parser.parse_verb, [('noun', 'princess'), ('verb', 'run'), ('direction', 'north')])

def test_parse_obj():
	assert_equal(parser.parse_object([('stop', 'princess'), ('noun', 'bear'), ('direction', 'north')]), ('noun', 'bear'))
	assert_equal(parser.parse_object([('stop', 'princess'), ('direction', 'north')]), ('direction', 'north'))
	assert_raises(parser.ParserError, parser.parse_object, [('verb', 'princess'), ('verb', 'run'), ('direction', 'north')])
	
def test_parse_subject():
	assert_equal(parser.parse_subject([('noun', 'princess'), ('verb', 'run'), ('direction', 'north')]), ('noun', 'princess'))
	assert_equal(parser.parse_subject([('stop', 'princess'), ('verb', 'run'), ('direction', 'north')]), ('noun', 'player'))
	assert_raises(parser.ParserError, parser.parse_subject, [('stop', 'princess'), ('stop', 'run'), ('direction', 'north')])

	
def test_parse_sentence():
	result = parser.parse_sentence([('noun', 'princess'), ('verb', 'runs'), ('direction', 'north')])
	assert_equal(result.subject, 'princess')
	assert_equal(result.verb, 'runs')
	assert_equal(result.object, 'north')

