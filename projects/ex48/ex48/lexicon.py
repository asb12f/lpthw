# this code creates a set of tuples
word1 = ('direction', 'north')
word2 = ('direction', 'south')
word3 = ('direction', 'east')
word4 = ('direction', 'west')
word5 = ('verb', 'go')
word6 = ('verb', 'kill')
word7 = ('verb', 'eat')
word8 = ('stop', 'in')
word9 = ('stop', 'the')
word10 = ('stop', 'of')
word11 = ('noun', 'bear')
word12 = ('noun', 'princess')

# this code adds our tuples to a list called "wordList"
wordList = [word1, word2, word3, word4, word5, word6, word7, word8, word9, word10, word11, word12]

#
def scan(sentence):
	"""the scan function breaks a sentence into a series of words, then creates a list of all Tuples matching those words"""
	matches = []
	k = 0
	words = sentence.split()
	while k < len(words):
		try:
			i = ('number', int(words[k]))
			matches.append(i)
		except ValueError:			
			for i in wordList:
				if i[1] == words[k].lower():
					matches.append(i)
		try:
			matches[k]
		except:
			matches.append(('error', words[k]))
		k+=1
	return matches	
