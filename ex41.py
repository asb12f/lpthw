## Animal is-a object (yes, sort of confusing), Look at the extra credit

class Animal(object):
	pass
	
## A dog is-a type of animal
class Dog(Animal):

	def __init__(self, name):
		## assigns a name to a dog
		self.name = name
		
## A cat is-a type of animal
class Cat(Animal):

	def __init__(self, name):
		##??
		self.name = name
		
## A person is a type of object
class Person(object):

	def __init__(self, name):
		## ??
		self.name = name
		
		## Person has-a pet of some kind
		self.pet = None
		
## An employee is a type of person
class Employee(Person):

	def __init__(self, name, salary):
		## An employee is a person who has a name
		super(Employee, self).__init__(name)
		## An empoyee has a salary
		self.salary = salary
		
## A fish is an object
class Fish(object):
	pass
	
## A salmon is a type of fish
class Salmon(Fish):
	pass
	
## A halibut is a type of fish
class Halibut(Fish):
	pass
	
## rover is-a Dog
rover = Dog("Rover")

## satan is a cat
satan = Cat("Satan")

## mary is a person
mary = Person("Mary")

## satan is mary's pet
mary.pet = satan

## frank is an employee who has a name "Frank" and a salary "120000"
frank = Employee("Frank", 120000)

## frank has a pet rover
frank.pet = rover

## flipper is a fish
flipper = Fish()

## crouse is a salmon
crouse = Salmon()

## harry is a halibut
harry = Halibut()
