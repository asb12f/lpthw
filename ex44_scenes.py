# This files contains a list of all scenes included in the game

class Death(Scene):
	
	quips = [
		"Have you considered playing something easier?",
		"Seriously, you aren't even trying.",
		"No really, try again.",
		"You thought that was going to work why?",
		"I have no words to describe how poor that decision was."
	]
	
	def enter(self):
		print Death.quips[randint(0, len(self.quips) - 1)]
		exit(1)
		
class Set1(Scene):

    def enter(self):
        print "The Gothons of Planet Percal #25 have invaded your ship and destroyed"

        action = raw_input("> ")

        if action == "shoot!":
            print "Quick on the draw you yank out your blaster and fire it at the Gothon."
            print "His clown costume is flowing and moving around his body, which throws"

        else:
            print "DOES NOT COMPUTE!"
            return 'central_corridor'

class Finished(Scene):

    def enter(self):
        print "You won! Good job."
        return 'finished'