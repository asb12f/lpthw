# create a variable for cars and assign it a value of 100
cars = 100
# assign the floating number value 4.0 to the space in a car
space_in_a_car = 4
# define the number of drivers and passengers
drivers = 30
passengers = 90
# calculates the number of cars that cannot have drivers
cars_not_driven = cars - drivers
# counts the number of cars that can be driven
cars_driven = drivers
# calculates the total carpooling capacity
carpool_capacity = cars_driven * space_in_a_car
# calculates the average number of passengers each car must take to ensure all passengers have a ride
average_passengers_per_car = passengers / cars_driven

print "There are", cars, "cars available."
print "There are only", drivers, "drivers available."
print "There will be", cars_not_driven, "empty cars."
print "We can transport", carpool_capacity, "people today."
print "We have", passengers, "to carpool today."
print "We need to put about", average_passengers_per_car, "in each car."